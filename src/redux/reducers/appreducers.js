const initialStae = {
    //studentloginstate
    studentloginid: '',
    studentloginstatus: '',
    studentloginMessage: '',
    studentData: [],
    studentClassesData: [],
    studentCourseData: [],
    studentVideoData: [],
    appdarkmodestate: false,
    Teacherdashboarddata: [],
    AllCourse: [],
    TeacherOtherInformation:[]
  };
  const reducer = (state = initialStae, action) => {
    switch (action.type) {
      case 'TEACHER_OTHERINFORMATION_DATA':
        return {...state, TeacherOtherInformation: action.payload};
    }
    return state;
  };
  export default reducer;